#! /usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import codecs
import time
import xlrd
from xlwt import Workbook, Formula

init_time=time.time()

""" loads conf """
try:
	fichier=open("youtube_info.json")
	fichier_json=json.load(fichier)
	fichier.close()
	key=fichier_json['key']
	channel_id=fichier_json['id']
except Exception as e:
	key="AIzaSyB5FHGt0wXkUFRW2Q842Q_KJeOnvBdiCrA" # ça c'est ma clé donc si tu veux faire des folies c'est elle qu'il faut utiliser
	channel_id="UCgvqvBoSHB1ctlyyhoHrGwQ" # ça c'est la chaîne d'Amixem

""" Returns time into the wented format """
def makeup(temps):
	s_len=len(temps)
	if temps[s_len-1]=="S":
		temps=str(temps[2:s_len-1])
		if len(temps)<3:
			temps="1min"
	else:
		temps=str(temps[2:s_len])
	temps=temps.replace('H','h')
	temps=temps.replace('M','min')
	while temps[len(temps)-1]!='n' and temps[len(temps)-1]!='h':
		temps=temps[0:len(temps)-1]
	return temps


""" Follows the same logic than the main requests described below """
def duree(vid):
	r_url="https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id="+vid+"&key="+key
	r_result=requests.get(r_url).text
	r_fic=codecs.open("video.json","w+","utf-8")
	r_fic.write(r_result)
	r_fic.close()
	r_json=open("video.json")
	r_res=json.load(r_json)
	r_json.close()
	temps=r_res['items'][0]['contentDetails']['duration']
	temps=makeup(temps)
	return temps



""" Initializates sheet """
fichier_xls="test.xls" # si tu veux spécifier un chemin absolu tu peux envisager de le faire là
tableur = Workbook()
feuille = tableur.add_sheet(channel_id)

""" Where you get all the info """
url="https://www.googleapis.com/youtube/v3/search?key="+key+"&channelId="+channel_id+"&part=snippet&order=date&maxResults=50"
result=requests.get(url).text
""" Puts the info into a file """
file=codecs.open("test.json","w+","utf-8")
file.write(result)
file.close()

""" Gets that same file into a json extension """ # Quand je tentais de faire la conversion directement ça m'ajoutait plein de caractères spéciaux et autres trucs chiants comme l'encodage utf-8 qui fonctionne pas
json_file=open("test.json")
json_result=json.load(json_file)
json_file.close()

taille = len(json_result['items']) # nombre de vidéos trouvées
inc=0 	# Ça servira au moment de faire les itérations pour ne pas laisser de ligne vide dans le tableur


""" Writes all the request's results into the sheet """
for x in xrange(0,taille):
	if json_result['items'][x]['id']['kind']=="youtube#video": 	# Permet de ne récupérer que les vidéos
		date=json_result['items'][x]['snippet']['publishedAt'][8:10]+"/"+json_result['items'][x]['snippet']['publishedAt'][5:7]+"/"+json_result['items'][x]['snippet']['publishedAt'][0:4] # Pour avoir la date au bon format
		video_id=json_result['items'][x]['id']['videoId']
		feuille.write(x+inc,0,json_result['items'][x]['snippet']['title'])
		feuille.write(x+inc,1,date)
		feuille.write(x+inc,2,duree(video_id)) # Récupère la durée de la vidéo (représente la quasi-totalité du temps de traitement)
	else:
		inc=inc-1 # Si c'est pas une vidéo on ne remplit pas la ligne donc il faudra pas incrémenter

""" Gets the time where the oldest video gotten has been published to consider only the older videos for the next request """
last_time = json_result['items'][taille-1]['snippet']['publishedAt'][0:19]
last_vid_time=(last_time[0:4],last_time[5:7],last_time[8:10],last_time[11:13],last_time[14:16],last_time[17:19])
time_setting = "&publishedBefore="+last_vid_time[0]+"-"+last_vid_time[1]+"-"+last_vid_time[2]+"T"+last_vid_time[3]+"%"+"3A"+last_vid_time[4]+"%3A"+last_vid_time[5]+"Z"


""" Makes the same thing until there is no more video to consider on the channel """
while taille>10: # 10 est totalement arbitraire, si on a moins de 50 résultats c'est que ce sont les dernières vidéos
	print taille
	inc=inc+taille-1 
	result=requests.get(url+time_setting).text 	# Effectue la nouvelle requête avec le paramètre de temps

	file=codecs.open("test.json","w+","utf-8")
	file.write(result)
	file.close()

	json_file=open("test.json")
	json_result=json.load(json_file)
	json_file.close()

	taille = len(json_result['items']) # nombre de vidéos trouvées

	for x in xrange(1,taille): # on commence à 1 pour éviter les doublons (le "&publishedBefore" inclut ses bornes donc la dernière vidéo de la requête précédente est également la première de la requête à venir), normalement ça permet même de récupérer plusieurs vidéos qui auraient été publiées dans la même seconde (je peux pas confirmer j'ai jamais rencontré le cas)
		if json_result['items'][x]['id']['kind']=="youtube#video":
			date=json_result['items'][x]['snippet']['publishedAt'][8:10]+"/"+json_result['items'][x]['snippet']['publishedAt'][5:7]+"/"+json_result['items'][x]['snippet']['publishedAt'][0:4]
			video_id=json_result['items'][x]['id']['videoId']
			feuille.write(x+inc,0,json_result['items'][x]['snippet']['title'])
			feuille.write(x+inc,1,date)
			feuille.write(x+inc,2,duree(video_id))
		else:
			inc=inc-1 	# je sais j'aurais pu inclure le premier for dans le while mais j'ai jamais dit que je faisais les choses bien

	last_time = json_result['items'][taille-1]['snippet']['publishedAt'][0:19]
	last_vid_time=(last_time[0:4],last_time[5:7],last_time[8:10],last_time[11:13],last_time[14:16],last_time[17:19])

	time_setting = "&publishedBefore="+last_vid_time[0]+"-"+last_vid_time[1]+"-"+last_vid_time[2]+"T"+last_vid_time[3]+"%"+"3A"+last_vid_time[4]+"%3A"+last_vid_time[5]+"Z"


tableur.save(fichier_xls)

end_time = time.time()

print "Temps d'execution : "+str(int(end_time-init_time))+" sec"